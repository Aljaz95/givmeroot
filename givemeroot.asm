; Simple give me root code, make sure you chmod +s the file (as root).
SECTION .text
global main
extern printf
extern setuid
extern system
main:
	push    ebp
	mov     ebp, esp
        sub     esp, 12      
        mov   eax,  [ebp + 8]; argc
        mov   ebx,  [ebp + 12] ; argv
 
        cmp eax, 1 ; make sure theres an  argument present.
        je .noArguments
        
        push 0        ; we set the uid to 0
        call setuid   

        push dword [ebx + 4] ; holds arg addy
        push arg      ; push the message
        call printf   ; call the printf system call;
        push dword [ebx + 4] ; holds arg addy
        mov  edi, ebx ; move argument to dest register;
        call system   ; we call the system
       
        jmp .restore

.noArguments:      
        push    noArg  
        call    printf   
.restore:
	mov     esp, ebp 
	pop     ebp
	ret
